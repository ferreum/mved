# This file is part of mved, the bulk file renaming tool.
# License: GNU GPL version 3, see the file "AUTHORS" for details.

"""Virtual filesystem for testing."""

import sys
from shutil import SameFileError

from mved.fsbase import FsBase


SYMLINK_RECURSION_LIMIT = 1000


def canonicalize_path_parts(parts):
    res = []
    for p in parts:
        if p not in ('', '.'):
            if p == '..':
                if res:
                    res.pop(-1)
            else:
                res.append(p)
    return res


def tokenize_path(path):
    return [t for t in path.split('/') if t]


class VirtFile(object):

    ty = 'file'

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return f"<{type(self).__name__} {self.ty} {self.name!r}>"

    def copy(self):
        return self.copy_into(VirtFile(self.name))

    def copy_into(self, other):
        return other

    def dump(self, text="", file=sys.stdout):
        print(f"{text or '/'} {self.ty} {self.name}", file=file)
        if self.ty == 'dir':
            for n, f in sorted(self.children.items()):
                if n not in ('.', '..'):
                    f.dump(text=(text + '/' + n), file=file)


class NoneFile(VirtFile):

    ty = 'none'

    def __init__(self):
        self.name = None

    def copy(self):
        raise FileNotFoundError("File does not exist")

    def __bool__(self):
        return False


NONEFILE = NoneFile()


class VirtLink(VirtFile):

    ty = 'link'

    def __init__(self, dest):
        VirtFile.__init__(self, dest)
        self.dest = dest

    def copy(self):
        return self.copy_into(VirtLink(self.dest))


class VirtDir(VirtFile):

    ty = 'dir'

    def __init__(self, name):
        VirtFile.__init__(self, name)
        self.children = {'..': self, '.': self}

    def copy(self):
        raise IsADirectoryError("Is a directory")

    def linked(self, parent):
        self.children['..'] = parent

    def unlinked(self):
        self.children['..'] = self

    def parents(self, end=NONEFILE):
        current = self
        while current is not end:
            yield current
            current = current.children['..']


class Traverse(object):

    def __init__(self, fs, path, parent=NONEFILE, relative_to=NONEFILE):
        if path.startswith('/'):
            relative_to = fs.root
        elif not relative_to:
            relative_to = fs._cwd
        stack = tokenize_path(path)
        stack.reverse()
        self.fs = fs
        self.stack = stack
        self.current = relative_to
        self.parent = parent or relative_to.children['..']

    def push_path(self, p):
        if p.startswith('/'):
            self.parent = self.fs.root
            self.current = self.fs.root
        else:
            if not p:
                raise OSError(f"Invalid argument: {p!r}")
            self.current = self.parent
        parts = tokenize_path(p)
        parts.reverse()
        self.stack.extend(parts)

    def follow(self):
        try:
            self.push_path(self.current.dest)
        except AttributeError:
            raise OSError("Not a link")

    def follow_now(self):
        stack = self.stack
        n = len(stack)
        self.follow()
        remain = SYMLINK_RECURSION_LIMIT
        for parent, current in self:
            if current.ty == 'link':
                self.follow()
                remain -= 1
                if remain <= 0:
                    raise RecursionError("Too many levels of symbolic links")
            elif len(stack) == n:
                return parent, current
        assert False, "unreachable"

    def __iter__(self):
        return self

    def __next__(self):
        stack = self.stack
        if not stack:
            raise StopIteration
        n = stack[-1]
        current = self.current
        self.current_name = n
        try:
            children = current.children
        except AttributeError:
            raise NotADirectoryError(f"Not a directory: {current!r}")
        f = children.get(n, NONEFILE)
        del stack[-1]
        self.parent = current
        self.current = f
        return current, f


class VirtualFs(FsBase):

    # TODO allow optional arguments of os/shutil

    def __init__(self):
        self.root = VirtDir("/")
        self.dump = self.root.dump
        self._cwdstr = "/"
        self._cwd = self.root

    def handle_with_dir(self, path, relative_to=NONEFILE, dironly=False,
                        follow_symlinks=False):
        if path.endswith('/'):
            dironly = True
        if dironly:
            follow_symlinks = True
        trav = self.traverse(path, relative_to=relative_to)
        for parent, current in trav:
            if current.ty == 'link' and (follow_symlinks or trav.stack):
                parent, current = trav.follow_now()
            if not current:
                if dironly:
                    raise NotADirectoryError(f"Not a directory: {path!r}")
                elif trav.stack:
                    return NONEFILE, NONEFILE
                else:
                    return parent, NONEFILE
        if dironly and trav.current.ty != 'dir':
            raise NotADirectoryError(f"Not a directory: {path!r}")
        return trav.parent, trav.current

    def handle(self, path, relative_to=NONEFILE,
               dironly=False, follow_symlinks=False):
        return self.handle_with_dir(path, relative_to=relative_to,
                                    dironly=dironly,
                                    follow_symlinks=follow_symlinks)[1]

    def traverse(self, path, parent=NONEFILE, relative_to=NONEFILE):
        return Traverse(self, path, parent=parent, relative_to=relative_to)

    def chdir(self, d):
        new = self.handle(d)
        if not new:
            raise FileNotFoundError(f"No such file or directory: {d!r}")
        if new.ty != 'dir':
            raise NotADirectoryError(f"Not a directory: {d!r}")
        toks = tokenize_path(self._cwdstr)
        if d.startswith('/'):
            toks.clear()
        toks.extend(tokenize_path(d))
        self._cwdstr = "/" + '/'.join(canonicalize_path_parts(toks))
        self._cwd = new

    def getcwd(self):
        return self._cwdstr

    def _follow_symlink(self, parent, link):
        try:
            return self.traverse(
                "", parent=parent, relative_to=link
            ).follow_now()[1]
        except NotADirectoryError:
            return NONEFILE

    def listdir(self, p):
        d = self.handle(p, dironly=True)
        return [f for f in d.children if f not in ('.', '..')]

    def fwalk(self, p, follow_symlinks=False):
        def _fwalk(thisdir, dirpath):
            files = []
            dirs = []
            enter_dirs = []
            for name, child in thisdir.children.items():
                if name not in ('.', '..'):
                    c = child
                    if c.ty == 'link':
                        c = self._follow_symlink(thisdir, c)
                    if c.ty == 'dir':
                        if follow_symlinks or child.ty == 'dir':
                            enter_dirs.append((name, child))
                        dirs.append(name)
                    else:
                        files.append(name)
            yield dirpath, dirs, files, thisdir
            if not dirpath.endswith('/'):
                dirpath += "/"
            for name, child in enter_dirs:
                yield from _fwalk(child, dirpath + name)
        thisdir = self.handle(p, follow_symlinks=follow_symlinks)
        if thisdir.ty == 'dir':
            yield from _fwalk(thisdir, p)

    def walk(self, p, followlinks=False):
        for dirpath, dirs, files, thisdir in self.fwalk(p):
            yield dirpath, dirs, files

    def exists(self, p):
        try:
            return bool(self.handle(p, follow_symlinks=True))
        except NotADirectoryError:
            return False

    def isdir(self, p):
        try:
            return self.handle(
                p, follow_symlinks=True, dironly=True
            ).ty == 'dir'
        except NotADirectoryError:
            return False

    def isfile(self, p):
        try:
            return self.handle(p, follow_symlinks=True).ty == 'file'
        except NotADirectoryError:
            return False

    def islink(self, p):
        try:
            return self.handle(p, follow_symlinks=False).ty == 'link'
        except NotADirectoryError:
            return False

    def makedirs(self, p, exist_ok=False):
        trav = self.traverse(p)
        created = False
        traversed = ""
        for parent, f in trav:
            traversed += "/" + trav.current_name
            if not f:
                f = VirtDir(traversed)
                parent.children[trav.current_name] = f
                f.linked(trav.parent)
                trav.current = f
                created = True
            elif f.ty == 'link' and trav.stack:
                if not trav.follow_now()[1]:
                    raise FileNotFoundError(
                        f"No such file or directory: {traversed!r}")
            elif f.ty != 'dir':
                raise FileExistsError(f"File exists: {p!r}")
        if not created and not exist_ok:
            raise FileExistsError(f"File exists: {p!r}")
        return trav.current

    def symlink(self, s, d):
        if not s:
            raise FileNotFoundError(f"No such file or directory: {s!r}")
        if not d:
            raise FileNotFoundError(f"No such file or directory: {d!r}")
        dd, df = self.handle_with_dir(d)
        if df:
            raise FileExistsError(f"File exists: {d!r}")
        if not dd:
            raise FileNotFoundError(f"No such file or directory: {d!r}")
        link = VirtLink(s)
        dd.children[self.basename(d)] = link
        return link

    def link(self, s, d):
        sd, sf = self.handle_with_dir(s)
        if not sf:
            raise FileNotFoundError(f"No such file or directory: {s!r}")
        if sf.ty == 'dir':
            raise PermissionError(f"Operation not permitted: {s!r} -> {d!r}")
        dd, df = self.handle_with_dir(d)
        if df:
            raise FileExistsError(f"File exists: {d!r}")
        dd.children[self.basename(d)] = sf

    def move(self, s, d, copy=False):
        sd, sf = self.handle_with_dir(s)
        if not sf:
            raise FileNotFoundError(f"No such file or directory: {s!r}")
        sb = self.basename(s)
        dd, df = self.handle_with_dir(d)
        if dd.ty != 'dir':
            raise FileNotFoundError(f"No such file or directory: {d!r}")
        db = self.basename(d)
        if df:
            if copy and sf is df:
                raise SameFileError(f"{s!r} and {d!r} are the same file")
            if df.ty == 'dir':
                if df.children.get(sb, NONEFILE).ty == 'dir':
                    raise IsADirectoryError(
                        f"Is a directory: {d + '/' + sb!r}")
                dd, db = df, sb
            else:
                if sf.ty == 'dir':
                    raise NotADirectoryError(
                        f"Not a directory: {s!r} -> {d!r}")
        if copy:
            sf = sf.copy()
        else:
            if dd is self:
                # dir is already at destination
                return
            for parent in dd.parents():
                if parent is sf:
                    raise OSError(
                        f"Cannot move directory into subdirectory"
                        f" of itself: {s!r} -> {d!r}")
                if parent is self.root:
                    break
            del sd.children[sb]
        dd.children[db] = sf
        return db

    rename = move

    def copy(self, s, d):
        self.move(s, d, copy=True)

    def unlink(self, p, isdir=False):
        pb = self.basename(p)
        if isdir and pb == '.':
            raise OSError(f"Invalid argument: {p!r}")
        pd, pf = self.handle_with_dir(p)
        if not pf:
            raise FileNotFoundError(f"No such file or directory: {p!r}")
        if (pf.ty == 'dir') != isdir:
            if isdir:
                raise NotADirectoryError(f"Not a directory: {p!r}")
            else:
                raise IsADirectoryError(f"Is a directory: {p!r}")
        if isdir:
            if len(pf.children) != 2:
                raise OSError(f"Directory not empty: {p!r}")
            pf.unlinked()
        del pd.children[pb]

    def rmdir(self, p):
        self.unlink(p, isdir=True)

    def mkfile(self, name):
        base = self.basename(name)
        if base in ('.', '..') or name.endswith('/'):
            raise OSError(f"Invalid argument: {name!r}")
        fd, f = self.handle_with_dir(name, follow_symlinks=True)
        if not fd:
            raise FileNotFoundError(f"No such file or directory: {name!r}")
        if f:
            if f.ty == 'dir':
                raise IsADirectoryError(f"Is a directory: {name!r}")
        else:
            base = self.basename(name)
            f = VirtFile(name)
            fd.children[base] = f
        return f

    def realpath(self, patharg, dironly=False):
        p = patharg
        if not p.startswith('/'):
            p = self._cwdstr + "/" + p
        trav = self.traverse(p)
        parts = []
        for parent, current in trav:
            if current.ty == 'link':
                if current.dest.startswith('/'):
                    parts.clear()
                trav.follow()
            else:
                parts.append(trav.current_name)
            if not current or not current.ty == 'dir':
                break
        parts.extend(reversed(trav.stack))
        path = canonicalize_path_parts(parts)
        return "/" + '/'.join(path)

    def readlink(self, name):
        f = self.handle(name)
        if not f:
            raise FileNotFoundError(f"No such file or directory: {name!r}")
        try:
            return f.dest
        except AttributeError:
            raise OSError(f"Invalid argument: {name!r}")

    def move_temp(self, path, suffix='', prefix=''):
        def get_name():
            nonlocal i
            i += 1
            return dir + '/' + prefix + "vfstemp_" + str(i) + suffix

        dir, base = self.splitpath(self.realdir(path))
        i = 0
        tempname = get_name()
        while self.handle(tempname):
            tempname = get_name()

        self.move(path, tempname)
        return tempname

    def mirror(self, other, path):
        realpath = other.realpath(path)
        if self.exists(realpath):
            raise FileExistsError(f"File exists: {realpath!r}")
        for dirpath, dirnames, filenames in other.walk(realpath):
            self.makedirs(dirpath)
            for filename in filenames:
                full_name = dirpath + "/" + filename
                if other.islink(full_name):
                    self.symlink(other.readlink(full_name), full_name)
                else:
                    self.mkfile(full_name)


# vim:set sw=4 et:
