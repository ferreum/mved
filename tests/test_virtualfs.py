# This file is part of mved, the bulk file renaming tool.
# License: GNU GPL version 3, see the file "AUTHORS" for details.

import unittest

import tests.virtualfs as vfs

# TODO test for files containing '.', '..'
# TODO test for files named '.', '..'
# TODO test walking arout paths with /../
# TODO test symlinks used in other operations
# TODO test traverse


class CreateTest(unittest.TestCase):

    def setUp(self):
        self.fs = vfs.VirtualFs()

    def test_file(self):
        fs = self.fs
        fs.mkfile("/a1")
        fs.makedirs("/aa")
        fs.mkfile("/aa/a1")
        self.assertEqual(fs.root.children["a1"].ty, 'file')
        self.assertEqual(fs.root.children["aa"].children["a1"].ty, 'file')
        self.assertIsNot(fs.root.children["a1"],
                         fs.root.children["aa"].children["a1"])

    def test_file_errors(self):
        fs = self.fs
        fs.makedirs("/ff")
        self.assertRaises(OSError, fs.mkfile, "/.")
        self.assertRaises(OSError, fs.mkfile, "/..")
        self.assertRaises(OSError, fs.mkfile, "/ff/")
        self.assertRaises(OSError, fs.mkfile, "/ff/.")
        self.assertRaises(OSError, fs.mkfile, "/ff/..")

    def test_file_exists_dir(self):
        fs = self.fs
        f = fs.makedirs("/ff")
        self.assertRaises(IsADirectoryError, fs.mkfile, "/ff")
        self.assertIs(fs.root.children["ff"], f)

    def test_dir_leaf(self):
        fs = self.fs
        fs.makedirs("/zz")
        self.assertEqual(fs.root.children["zz"].ty, 'dir')

    def test_dir_path(self):
        fs = self.fs
        fs.makedirs("/xx/yy/zz")
        prev = fs.root
        for d in ("xx", "yy", "zz"):
            f = prev.children[d]
            self.assertEqual(f.ty, 'dir')
            self.assertIs(f.children['..'], prev, d)
            prev = f

    def test_dir_exists_file(self):
        fs = self.fs
        fs.makedirs("/aa")
        f = fs.mkfile("/aa/a1")
        self.assertRaises(FileExistsError, fs.makedirs, "/aa/a1")
        self.assertRaises(FileExistsError, fs.makedirs, "/aa/a1", exist_ok=True)
        self.assertIs(fs.root.children["aa"].children["a1"], f)

    def test_dir_exists_dir(self):
        fs = self.fs
        f = fs.makedirs("/aa")
        self.assertRaises(FileExistsError, fs.makedirs, "/aa")
        fs.makedirs("/aa", exist_ok=True)
        self.assertIs(fs.root.children["aa"], f)


class VirtualFsTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        if cls is VirtualFsTest:
            raise unittest.SkipTest("base test class")
        super(VirtualFsTest, cls).setUpClass()

    def setUp(self):
        fs = vfs.VirtualFs()
        def mkfile(name):
            fs.makedirs(fs.dirname(name), exist_ok=True)
            return fs.mkfile(name)
        self.assertNotEqual(type(self.files), str)
        self.assertNotEqual(type(self.dirs), str)
        self.created = list(map(mkfile, self.files))
        self.created.extend(map(fs.makedirs, self.dirs))
        self.fs = fs

    def test_build(self):
        def verify_dir(d):
            prev = fs.root
            rem = d
            cur = "/"
            while rem:
                head, sep, rem = rem.lstrip('/').partition('/')
                cur += "/" + head
                f = fs.handle(cur)
                self.assertEqual(fs.handle(cur).ty, 'dir')
                self.assertIs(fs.handle(cur + "/.."), prev)
                prev = f

        fs = self.fs
        ci = iter(self.created)
        for f in self.files:
            self.assertEqual(fs.handle(f).name, f)
            self.assertIs(fs.handle(f), next(ci))
        for f in self.dirs:
            self.assertEqual(fs.handle(f + "/").name, f)
            self.assertIs(fs.handle(f + "/"), next(ci))
            verify_dir(f)


class HandleTest(VirtualFsTest):

    files = ("/aa/a1",
             "/aa/bb/b1",
             "/aa/bb/b2")
    dirs = ()

    def test_simple(self):
        fs = self.fs
        self.assertEqual(fs.handle("/aa").name, "/aa")
        self.assertEqual(fs.handle("/aa/").name, "/aa")
        self.assertEqual(fs.handle("/aa/a1").name, "/aa/a1")
        self.assertEqual(fs.handle("/aa/bb/b1").name, "/aa/bb/b1")
        self.assertEqual(fs.handle("/aa/bb/b2").name, "/aa/bb/b2")
        self.assertEqual(fs.handle("/aa/bb/../a1").name, "/aa/a1")
        self.assertRaises(NotADirectoryError, fs.handle, "/aa/bb/b2/")

    def test_root(self):
        fs = self.fs
        self.assertEqual(fs.root, fs.handle("/"))
        self.assertEqual(fs.root, fs.handle("/."))
        self.assertEqual(fs.root, fs.handle("/.."))
        self.assertEqual(fs.root, fs.handle("/../../../"))
        self.assertEqual(fs.root, fs.handle("/.././././../."))
        self.assertEqual(fs.root, fs.handle("../.././././../."))

    def test_link_dironly(self):
        fs = self.fs
        link = fs.symlink("/aa/bb", "/ll")
        self.assertEqual(fs.handle("/ll"), link)
        self.assertEqual(fs.handle("/ll/"), fs.handle("/aa/bb"))


class HandleWithDirTest(VirtualFsTest):

    files = ("/aa/a1",
             "/aa/bb/b1",
             "/aa/bb/b2")
    dirs = ("/cc/dd",)

    def test_nonexisting_path(self):
        fs = self.fs
        res = fs.handle_with_dir("/xx/yy")
        self.assertIs(res[0], vfs.NONEFILE)
        self.assertIs(res[1], vfs.NONEFILE)

    def test_link_target(self):
        fs = self.fs
        fs.symlink("/aa/a1", "/cc/dd/x1")
        res = fs.handle_with_dir("/cc/dd/x1", follow_symlinks=True)
        self.assertIs(res[0], fs.handle("/aa"))
        self.assertIs(res[1], self.created[0])

    def test_link_path(self):
        fs = self.fs
        fs.symlink("/aa", "/cc/xx")
        res = fs.handle_with_dir("/cc/xx/a1", follow_symlinks=True)
        self.assertIs(res[0], fs.handle("/aa"))
        self.assertIs(res[1], self.created[0])


class ListdirTest(VirtualFsTest):

    files = ("/aa/a1",
             "/aa/a2",
             "/aa/bb/b1")
    dirs = ("/cc",)

    def test_simple(self):
        fs = self.fs
        self.assertSetEqual(set(fs.listdir("/")), {"aa", "cc"})
        self.assertSetEqual(set(fs.listdir(".")), {"aa", "cc"})
        self.assertSetEqual(set(fs.listdir("..")), {"aa", "cc"})
        self.assertSetEqual(set(fs.listdir("/aa")), {"a1", "a2", "bb"})
        self.assertSetEqual(set(fs.listdir("/aa/")), {"a1", "a2", "bb"})
        self.assertSetEqual(set(fs.listdir("/aa/.")), {"a1", "a2", "bb"})
        self.assertSetEqual(set(fs.listdir("/aa/bb/..")), {"a1", "a2", "bb"})

    def test_empty(self):
        fs = self.fs
        self.assertSetEqual(set(fs.listdir("/cc")), set())

    def test_errors(self):
        fs = self.fs
        self.assertRaises(NotADirectoryError, fs.listdir, "/aa/a1")
        self.assertRaises(NotADirectoryError, fs.listdir, "/aa/a1/")
        self.assertRaises(NotADirectoryError, fs.listdir, "/aa/a1/zz")
        self.assertRaises(NotADirectoryError, fs.listdir, "/aa/a1/.")
        self.assertRaises(NotADirectoryError, fs.listdir, "/aa/a1/..")
        self.assertRaises(NotADirectoryError, fs.listdir, "/aa/zz")


class FWalkTest(VirtualFsTest):

    files = ("/aa/a1", "/aa/a2", "/aa/bb/b1", "/cc/c1")
    dirs = ()

    path = "/"
    follow_symlinks = False
    expect = [("/", ["aa", "cc"], [], "/"),
              ("/aa", ["bb"], ["a1", "a2"], "/aa"),
              ("/aa/bb", [], ["b1"], "/aa/bb"),
              ("/cc", [], ["c1"], "/cc")]

    def create_expect(self, expect):
        return sorted([(p, set(d), set(f), self.fs.handle(h))
                       for p, d, f, h in expect])

    def create_result(self, gen):
        return sorted([(p, set(d), set(f), h)
                       for p, d, f, h in gen])

    def assertFWalkEquals(self, gen, expect):
        self.assertSequenceEqual(
                self.create_result(gen), self.create_expect(expect))

    def test_fwalk(self):
        fs = self.fs
        self.assertFWalkEquals(fs.fwalk(
            self.path,
            follow_symlinks=self.follow_symlinks), self.expect)


class FWalkEmptyDirsTest(FWalkTest):

    files = ("/aa/a1", "/aa/a2")
    dirs = ("/aa/bb", "/cc")

    path = "/"
    expect = [("/", ["aa", "cc"], [], "/"),
              ("/aa", ["bb"], ["a1", "a2"], "/aa"),
              ("/aa/bb", [], [], "/aa/bb"),
              ("/cc", [], [], "/cc")]


class FWalkSubdirTest(FWalkTest):

    path = "/aa"
    expect = [("/aa", ["bb"], ["a1", "a2"], "/aa"),
              ("/aa/bb", [], ["b1"], "/aa/bb")]


class FWalkSymlinkTest(FWalkTest):

    path = "/aa"
    expect = [("/aa", ["bb", "ll"], ["a1", "a2", "l1"], "/aa"),
              ("/aa/bb", [], ["b1"], "/aa/bb")]

    def test_fwalk(self):
        fs = self.fs
        fs.symlink("/cc", "/aa/ll")
        fs.symlink("/cc/c1", "/aa/l1")
        FWalkTest.test_fwalk(self)


class FWalkIntoSymlinkTest(FWalkSymlinkTest):

    path = "/aa/ll"
    follow_symlinks = True
    expect = [("/aa/ll", [], ["c1"], "/cc")]


class FWalkDeadSymlinkTest(FWalkTest):

    files = ("/aa/a1", "/aa/a2", "/aa/bb/b1", "/cc/c1")
    dirs = ()

    path = "/aa"
    expect = [("/aa", ["bb", "lc"], ["a1", "a2", "lx", "l1"], "/aa"),
              ("/aa/bb", [], ["b1"], "/aa/bb")]

    def test_fwalk(self):
        fs = self.fs
        fs.symlink("/xx", "/aa/lx")
        fs.symlink("/cc", "/aa/lc")
        fs.symlink("lc/x1", "/aa/l1")
        FWalkTest.test_fwalk(self)


class ChdirTest(VirtualFsTest):

    files = ("/aa/a1", "/aa/bb/b1")
    dirs = ("/cc",)

    def test_relative(self):
        fs = self.fs
        fs.chdir("aa")
        self.assertEqual(fs.getcwd(), "/aa")
        self.assertIs(fs._cwd, fs.handle("/aa"))
        fs.chdir("bb")
        self.assertEqual(fs.getcwd(), "/aa/bb")
        self.assertIs(fs._cwd, fs.handle("/aa/bb"))

    def test_absolute(self):
        fs = self.fs
        fs.chdir("/aa")
        self.assertEqual(fs.getcwd(), "/aa")
        self.assertIs(fs._cwd, fs.handle("/aa"))
        fs.chdir("/cc")
        self.assertEqual(fs.getcwd(), "/cc")
        self.assertIs(fs._cwd, fs.handle("/cc"))
        fs.chdir("/aa/bb")
        self.assertEqual(fs.getcwd(), "/aa/bb")
        self.assertIs(fs._cwd, fs.handle("/aa/bb"))


class PredicatesTest(VirtualFsTest):

    files = ("/aa/bb/a1",
             "/aa/bb/a2")
    dirs = ()

    def test_exists(self):
        fs = self.fs
        fs.symlink("/xx", "/lx")
        fs.symlink("/aa/bb", "/lb")
        self.assertTrue(fs.exists("/"))
        self.assertTrue(fs.exists("/aa"))
        self.assertFalse(fs.exists("/xx"))
        self.assertTrue(fs.exists("/lb/a1"))
        self.assertFalse(fs.exists("/lb/a1/"))
        self.assertFalse(fs.exists("/lx"))
        self.assertFalse(fs.exists("/lx/"))
        self.assertFalse(fs.exists("/lx/.."))
        self.assertFalse(fs.exists("/lx/../aa"))

    def test_isdir(self):
        fs = self.fs
        fs.symlink("/aa/bb", "/aa/ll")
        fs.symlink("/aa/bb/xx", "/aa/mm")
        self.assertTrue(fs.isdir("/"))
        self.assertTrue(fs.isdir("/../"))
        self.assertTrue(fs.isdir("/aa/bb"))
        self.assertTrue(fs.isdir("/aa/bb/"))
        self.assertFalse(fs.isdir("/aa/bb/a1"))
        self.assertFalse(fs.isdir("/yy/xx"))
        self.assertFalse(fs.isdir("/aa/bb/a1/zz"))
        self.assertFalse(fs.isdir("/aa/bb/a1/"))
        self.assertFalse(fs.isdir("/aa/bb/a1/."))
        self.assertFalse(fs.isdir("/aa/bb/a1/.."))
        self.assertTrue(fs.isdir("/aa/ll"))
        self.assertTrue(fs.isdir("/aa/ll/"))
        self.assertFalse(fs.isdir("/aa/mm"))
        self.assertFalse(fs.isdir("/aa/mm/"))

    def test_isfile(self):
        fs = self.fs
        fs.symlink("/aa/bb/a1", "/aa/l1")
        fs.symlink("/aa/xx", "/aa/m1")
        self.assertFalse(fs.isfile("/"))
        self.assertFalse(fs.isfile("/aa"))
        self.assertFalse(fs.isfile("/aa/bb/"))
        self.assertTrue(fs.isfile("/aa/bb/a1"))
        self.assertFalse(fs.isfile("/aa/bb/a1/zz"))
        self.assertFalse(fs.isfile("/aa/bb/a1/"))
        self.assertFalse(fs.isfile("/aa/bb/a1/."))
        self.assertFalse(fs.isfile("/aa/bb/a1/.."))
        self.assertTrue(fs.isfile("/aa/l1"))
        self.assertFalse(fs.isfile("/aa/m1"))

    def test_islink(self):
        fs = self.fs
        fs.symlink("/aa/bb/a1", "/aa/l1")
        fs.symlink("/aa/xx", "/aa/m1")
        fs.symlink("/aa/bb", "/aa/ll")
        self.assertTrue(fs.islink("/aa/l1"))
        self.assertTrue(fs.islink("/aa/m1"))
        self.assertTrue(fs.islink("/aa/ll"))
        self.assertFalse(fs.islink("/aa/ll/"))
        self.assertFalse(fs.islink("/aa"))
        self.assertFalse(fs.islink("/aa/"))


class SymlinkTest(VirtualFsTest):

    files = ("/aa/a1",
             "/aa/a2")
    dirs = ("/aa/bb",)

    def test_create(self):
        fs = self.fs
        fs.symlink("/aa/a1", "/aa/z1")
        self.assertEqual(fs.handle("/aa/z1").dest, "/aa/a1")
        self.assertEqual(fs.handle("/aa/z1").ty, 'link')

    def test_create_nonexisting(self):
        fs = self.fs
        fs.symlink("/xx/yy", "/aa/z1")
        self.assertEqual(fs.handle("/aa/z1").dest, "/xx/yy")
        self.assertEqual(fs.handle("/aa/z1").ty, 'link')

    def test_create_errors(self):
        fs = self.fs
        self.assertRaises(FileExistsError, fs.symlink, "/xx", "/aa")
        self.assertRaises(FileExistsError, fs.symlink, "/xx", "/aa/a1")
        self.assertRaises(FileExistsError, fs.symlink, "/xx", "/aa/.")
        self.assertRaises(FileExistsError, fs.symlink, "/xx", "/aa/..")
        self.assertRaises(FileNotFoundError, fs.symlink, "/xx", "/zz/yy")
        self.assertRaises(FileNotFoundError, fs.symlink, "", "/zz")
        self.assertRaises(FileNotFoundError, fs.symlink, "/xx", "")
        self.assertRaises(FileNotFoundError, fs.symlink, "", "")
        self.assertRaises(NotADirectoryError, fs.symlink, "/xx", "/aa/a1/")
        self.assertRaises(NotADirectoryError, fs.symlink, "/xx", "/aa/a1/zz")

    def test_create_dir(self):
        fs = self.fs
        link = fs.symlink("/xx", "/xl")
        self.assertRaises(FileExistsError, fs.makedirs, "/xl")
        self.assertRaises(FileNotFoundError, fs.makedirs, "/xl/yy")
        self.assertIs(fs.handle("/xx"), vfs.NONEFILE)
        self.assertIs(fs.handle("/xl"), link)


class FollowAbsSymlinkTest(VirtualFsTest):

    files = ("/aa/a1",
             "/aa/a2")
    dirs = ("/aa/bb",)

    def test_path(self):
        fs = self.fs
        fs.symlink("/aa", "/xx")
        self.assertIs(fs.handle("/xx/a1", follow_symlinks=False), fs.handle("/aa/a1"))
        self.assertIs(fs.handle("/xx/a1", follow_symlinks=True), fs.handle("/aa/a1"))

    def test_file(self):
        fs = self.fs
        fs.symlink("/aa/a1", "/aa/bb/z1")
        self.assertIs(fs.handle("/aa/bb/z1", follow_symlinks=True), fs.handle("/aa/a1"))
        self.assertIs(fs.handle("/aa/bb/z1", follow_symlinks=False).ty, 'link')
        self.assertIs(fs.handle("/aa/bb/z1", follow_symlinks=False).dest, "/aa/a1")

    def test_recurse(self):
        fs = self.fs
        fs.symlink("/aa", "/xx")
        fs.symlink("/xx", "/yy")
        fs.symlink("/yy", "/zz")
        self.assertEqual(fs.handle("/zz", follow_symlinks=False).dest, "/yy")
        self.assertIs(fs.handle("/zz", follow_symlinks=True), fs.handle("/aa"))
        self.assertIs(fs.handle("/zz/a1", follow_symlinks=False), fs.handle("/aa/a1"))
        self.assertIs(fs.handle("/zz/a1", follow_symlinks=True), fs.handle("/aa/a1"))

    def test_deep_recursion(self):
        # Assumes recursion limit of 1000.
        fs = self.fs
        p = "/aa"
        for i in range(1001):
            i = "/" + str(i)
            fs.symlink(p, i)
            p = i
        self.assertIs(fs.handle("/999", follow_symlinks=True), fs.handle("/aa"))
        self.assertRaises(RecursionError, fs.handle, "/1000", follow_symlinks=True)
        self.assertIs(fs.handle("/999/a1", follow_symlinks=True), fs.handle("/aa/a1"))
        self.assertRaises(RecursionError, fs.handle, "/1000/a1", follow_symlinks=True)

    def test_inf_recursion(self):
        fs = self.fs
        fs.symlink("/c1", "/c2")
        fs.symlink("/c2", "/c1")
        self.assertRaises(RecursionError, fs.handle, "/c1", follow_symlinks=True)


class FollowRelSymlinkTest(VirtualFsTest):

    files = ("/aa/a1",
             "/aa/a2")
    dirs = ("/aa/bb",)

    def test_recurse(self):
        fs = self.fs
        fs.symlink("../a1", "/aa/bb/y1")
        self.assertIs(fs.handle("/aa/bb/y1", follow_symlinks=True), fs.handle("/aa/a1"))


class LinkMoveTest(VirtualFsTest):

    files = ("/aa/a1",
             "/aa/a2",
             "/aa/ff",
             "/cc/a1")
    dirs = ("/aa/bb/ff",
            "/aa/cc/ff")

    def _test_link(self, func, s, d, exp, index, action):
        fs = self.fs
        func(s, d)
        f = self.created[index]
        if action == 'copy':
            self.assertIsNot(fs.handle(exp), f)
            self.assertEqual(fs.handle(exp).name, f.name)
            self.assertIs(fs.handle(s), f)
        else:
            self.assertIs(fs.handle(exp), f)
            if action == 'move': # else 'link'
                f = vfs.NONEFILE
            self.assertIs(fs.handle(s), f)


class LinkTest(LinkMoveTest):

    def test_simple(self):
        self._test_link(self.fs.link, "/aa/a1", "z1", "z1", 0, action='link')

    def test_errors(self):
        fs = self.fs
        self.assertRaises(FileNotFoundError, fs.link, "/yy/xx", "z1")
        self.assertRaises(FileNotFoundError, fs.link, "/aa/xx", "z1")
        self.assertRaises(PermissionError, fs.link, "/aa/bb", "z1")
        self.assertRaises(FileExistsError, fs.link, "/aa/a1", "/aa/a2")
        self.assertRaises(FileExistsError, fs.link, "/aa/a1", "/aa/bb")
        fs.link("/aa/a1", "z1")


class MoveTest(LinkMoveTest):

    COPY = False

    def _test_move(self, *args):
        func = self.fs.copy if self.COPY else self.fs.move
        action = 'copy' if self.COPY else 'move'
        self._test_link(func, *args, action=action)

    def test_simple(self):
        self._test_move("/aa/a1", "/aa/a1x", "/aa/a1x", 0)

    def test_todir(self):
        self._test_move("/aa/a2", "/aa/bb", "/aa/bb/a2", 1)

    def test_overwrite(self):
        self._test_move("/aa/a1", "/aa/a2", "/aa/a2", 0)

    def test_overwrite_todir(self):
        self._test_move("/aa/a1", "/aa/cc", "/aa/cc/a1", 0)

    def test_errors(self):
        fs = self.fs
        func = fs.copy if self.COPY else fs.move
        self.assertRaises(FileNotFoundError, func, "/xx/yy", "z1")
        self.assertRaises(FileNotFoundError, func, "/aa/yy", "z1")
        self.assertRaises(NotADirectoryError, func, "/aa/a1/xx", "z1")
        self.assertRaises(FileNotFoundError, func, "/aa/a1", "/xx/yy/z1")
        self.assertRaises(NotADirectoryError, func, "/aa/a1", "/aa/a2/aa")
        self.assertRaises(IsADirectoryError, func, "/aa/ff", "/aa/cc")
        self.assertRaises(IsADirectoryError, func, "/aa/bb/ff", "/aa/cc")
        self.assertRaises(NotADirectoryError, func, "/aa/bb/ff", "/aa/a1")
        fs.move("/aa/a1", "z1")


class CopyTest(MoveTest):

    COPY = True


class MoveDirTest(VirtualFsTest):

    files = ()
    dirs = ("/aa/bb/cc", "/dd/ee")

    def setUp(self):
        VirtualFsTest.setUp(self)
        fs = self.fs
        fs.symlink("/aa/bb", "/bb")

    def test_rename(self):
        fs = self.fs
        fs.move("/dd/ee", "/dd/xx")
        self.assertIs(fs.handle("/dd/xx"), self.created[1])

    def test_move_into_self(self):
        fs = self.fs
        self.assertRaises(OSError, fs.move, "/", "/aa")
        self.assertRaises(OSError, fs.move, "/aa", "/aa/bb")
        self.assertRaises(OSError, fs.move, "/aa", "/bb")
        self.assertRaises(OSError, fs.move, "/aa", "/aa")


class UnlinkTest(VirtualFsTest):

    files = ("/aa/a1", "/aa/a2")
    dirs = ("/aa/bb", "/cc")

    def test_simple(self):
        fs = self.fs
        fs.unlink("/aa/a1")
        self.assertIs(fs.handle("/aa/a1"), vfs.NONEFILE)

    def test_dir(self):
        fs = self.fs
        fs.rmdir("/aa/bb")
        self.assertIs(fs.handle("/aa/bb"), vfs.NONEFILE)

    def test_dir_slash(self):
        fs = self.fs
        fs.rmdir("/aa/bb/")
        self.assertIs(fs.handle("/aa/bb"), vfs.NONEFILE)

    def test_errors(self):
        fs = self.fs
        self.assertRaises(OSError, fs.unlink, "/.")
        self.assertRaises(OSError, fs.unlink, "/..")
        self.assertRaises(OSError, fs.unlink, "/aa/.")
        self.assertRaises(OSError, fs.unlink, "/aa/..")
        self.assertRaises(FileNotFoundError, fs.unlink, "/yy/zz")
        self.assertRaises(FileNotFoundError, fs.unlink, "/aa/zz")
        self.assertRaises(IsADirectoryError, fs.unlink, "/aa/bb")
        self.assertRaises(NotADirectoryError, fs.unlink, "/aa/a1/zz")
        self.assertRaises(NotADirectoryError, fs.unlink, "/aa/a1/")
        self.assertRaises(NotADirectoryError, fs.rmdir, "/aa/a1")
        self.assertRaises(NotADirectoryError, fs.rmdir, "/aa/a1/")
        self.assertRaises(OSError, fs.rmdir, "/aa")
        self.assertRaises(OSError, fs.rmdir, "/aa/..")
        fs.unlink("/aa/a1")


class RealpathTest(VirtualFsTest):

    files = ("/aa/a1", "/aa/a2", "/cc/c1")
    dirs = ("/aa/bb",)

    def test_simple(self):
        fs = self.fs
        self.assertEqual(fs.realpath("/aa/a1"), "/aa/a1")
        self.assertEqual(fs.realpath("/aa/bb"), "/aa/bb")

    def test_root(self):
        fs = self.fs
        self.assertEqual(fs.realpath("."), "/")
        self.assertEqual(fs.realpath(".."), "/")
        self.assertEqual(fs.realpath("/"), "/")
        self.assertEqual(fs.realpath("/."), "/")
        self.assertEqual(fs.realpath("/.."), "/")
        self.assertEqual(fs.realpath("/./"), "/")
        self.assertEqual(fs.realpath("/../"), "/")

    def test_link(self):
        fs = self.fs
        fs.symlink("a1", "/aa/l1")
        fs.symlink("/aa/a2", "/l2")
        self.assertEqual(fs.realpath("/aa/a1"), "/aa/a1")
        self.assertEqual(fs.realpath("/aa/bb"), "/aa/bb")
        self.assertEqual(fs.realpath("/aa/l1"), "/aa/a1")
        self.assertEqual(fs.realpath("/l2"), "/aa/a2")

    def test_link_up(self):
        fs = self.fs
        fs.symlink("../cc", "/aa/cc")
        self.assertEqual(fs.realpath("/aa/cc"), "/cc")
        self.assertEqual(fs.realpath("/aa/cc/c1"), "/cc/c1")

    def test_abslink(self):
        fs = self.fs
        fs.symlink("/cc", "/aa/cc")
        self.assertEqual(fs.realpath("/aa/cc"), "/cc")
        self.assertEqual(fs.realpath("/aa/cc/c1"), "/cc/c1")

    def test_nonexisting(self):
        fs = self.fs
        self.assertEqual(fs.realpath("/xx/yy/zz"), "/xx/yy/zz")
        self.assertEqual(fs.realpath("/aa/zz"), "/aa/zz")
        self.assertEqual(fs.realpath("/aa/zz/.."), "/aa")
        self.assertEqual(fs.realpath("/aa/zz/../.."), "/")
        self.assertEqual(fs.realpath("/xx", dironly=True), "/xx")
        self.assertEqual(fs.realpath("/xx/"), "/xx")

    def test_nonexisting_link(self):
        fs = self.fs
        fs.symlink("xx", "/aa/gg")
        fs.symlink("/xx", "/aa/ff")
        self.assertEqual(fs.realpath("/aa/gg"), "/aa/xx")
        self.assertEqual(fs.realpath("/aa/gg/zz"), "/aa/xx/zz")
        self.assertEqual(fs.realpath("/aa/gg/yy/../zz"), "/aa/xx/zz")
        self.assertEqual(fs.realpath("/aa/gg/../zz"), "/aa/zz")
        self.assertEqual(fs.realpath("/aa/ff/yy/../zz"), "/xx/zz")
        self.assertEqual(fs.realpath("/aa/ff/../zz"), "/zz")

    def test_chdir(self):
        fs = self.fs
        fs.chdir("/aa")
        self.assertEqual(fs.realpath("bb"), "/aa/bb")
        self.assertEqual(fs.realpath("../cc"), "/cc")

    def test_sub_of_nondir(self):
        fs = self.fs
        self.assertEqual(fs.realpath("/aa/a1/x1"), "/aa/a1/x1")


class RealdirTest(VirtualFsTest):

    files = ("/aa/a1", "/aa/a2", "/cc/c1")
    dirs = ("/aa/bb",)

    def test_simple(self):
        fs = self.fs
        self.assertEqual(fs.realdir("/aa/a1"), "/aa/a1")
        self.assertEqual(fs.realdir("/aa"), "/aa")

    def test_inroot(self):
        fs = self.fs
        self.assertEqual(fs.realdir("/aa"), "/aa")
        self.assertEqual(fs.realdir("/etc"), "/etc")

    def test_inroot_chdir(self):
        fs = self.fs
        fs.chdir("cc")
        self.assertEqual(fs.realdir("/aa"), "/aa")
        self.assertEqual(fs.realdir("/etc"), "/etc")

    def test_nonexisting(self):
        fs = self.fs
        self.assertEqual(fs.realdir("/aa/xx/"), "/aa/xx")
        self.assertEqual(fs.realdir("/aa/xx/yy"), "/aa/xx/yy")

    def test_link(self):
        fs = self.fs
        fs.symlink("/aa/a1", "/aa/bb/l1")
        fs.symlink("/aa", "/cc/aa")
        self.assertEqual(fs.realdir("/cc/aa/a1"), "/aa/a1")
        self.assertEqual(fs.realdir("/cc/aa/xx"), "/aa/xx")
        self.assertEqual(fs.realdir("/cc/aa/bb/l1"), "/aa/bb/l1")

    def test_chdir(self):
        fs = self.fs
        fs.symlink("/aa/a1", "/aa/bb/l1")
        fs.chdir("aa")
        self.assertEqual(fs.realdir("a1"), "/aa/a1")
        fs.chdir("bb")
        self.assertEqual(fs.realdir("l1"), "/aa/bb/l1")

    def test_errors(self):
        fs = self.fs
        self.assertRaises(OSError, fs.realdir, "/aa/a1/.")
        self.assertRaises(OSError, fs.realdir, "/aa/a1/..")
        self.assertRaises(OSError, fs.realdir, "/aa/a1/../")


class ReadlinkTest(VirtualFsTest):

    files = ("/aa/a1", "/aa/a2")
    dirs = ("/aa/bb", "/cc")

    def test_simple(self):
        fs = self.fs
        fs.symlink("/aa/a1", "/l1")
        fs.symlink("../l1", "/cc/ll1")
        fs.symlink("../xx", "/cc/lx")
        self.assertEqual(fs.readlink("/l1"), "/aa/a1")
        self.assertEqual(fs.readlink("/cc/ll1"), "../l1")
        self.assertEqual(fs.readlink("/cc/lx"), "../xx")

    def test_errors(self):
        fs = self.fs
        fs.symlink("/xx", "/l1")
        self.assertRaises(FileNotFoundError, fs.readlink, "/xx")
        self.assertRaises(OSError, fs.readlink, "/aa")
        self.assertRaises(OSError, fs.readlink, "/aa/a1")


class MoveTempTest(VirtualFsTest):

    files = ("/aa/a1", "/aa/a2")
    dirs = ()

    def test_simple(self):
        fs = self.fs
        tempname1 = fs.move_temp("/aa/a1")
        tempname2 = fs.move_temp("/aa/a2")
        self.assertIs(fs.handle("/aa/a1"), vfs.NONEFILE)
        self.assertIs(fs.handle("/aa/a2"), vfs.NONEFILE)
        self.assertIs(fs.handle(tempname1), self.created[0])
        self.assertIs(fs.handle(tempname2), self.created[1])
        dir1, name1 = fs.splitpath(tempname1)
        dir2, name2 = fs.splitpath(tempname2)
        self.assertEqual(dir1, '/aa')
        self.assertEqual(dir2, '/aa')

    def test_prefix_suffix(self):
        fs = self.fs
        prefix = ".my_temp_"
        suffix = ".test"
        tempname1 = fs.move_temp("/aa/a1", prefix=prefix, suffix=suffix)
        tempname2 = fs.move_temp("/aa/a2", prefix=prefix, suffix=suffix)
        self.assertIs(fs.handle("/aa/a1"), vfs.NONEFILE)
        self.assertIs(fs.handle("/aa/a2"), vfs.NONEFILE)
        self.assertIs(fs.handle(tempname1), self.created[0])
        self.assertIs(fs.handle(tempname2), self.created[1])
        dir1, name1 = fs.splitpath(tempname1)
        dir2, name2 = fs.splitpath(tempname2)
        self.assertEqual(dir1, '/aa')
        self.assertEqual(dir2, '/aa')
        self.assertTrue(name1.startswith(prefix), repr(name1))
        self.assertTrue(name1.endswith(suffix), repr(name1))
        self.assertTrue(name2.startswith(prefix), repr(name2))
        self.assertTrue(name2.endswith(suffix), repr(name2))


# vim:set sw=4 et:
