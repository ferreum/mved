# This file is part of mved, the bulk file renaming tool.
# License: GNU GPL version 3, see the file "AUTHORS" for details.

import unittest
from unittest.mock import Mock, patch, ANY, sentinel

from mved import MvEd, ApplyContext
from tests.virtualfs import VirtualFs
import mved.menu as menu
from mved.menu import Menu


APPLIER_SPEC = ApplyContext(Mock(spec=VirtualFs), [])


class ReadchoiceTest(unittest.TestCase):

    @patch('mved.menu.input')
    def _test(self, input, choices,
              exp_choice, exp_upper, exp_instr,
              m_input, hchoices=[]):
        m_input.side_effect = [input]
        choice, isupper, instr = menu.readchoice(choices, hchoices=hchoices)
        self.assertTrue(m_input.called)
        self.assertEqual(choice, exp_choice)
        self.assertEqual(isupper, exp_upper)
        self.assertEqual(instr, exp_instr)

    def test_simple(self):
        self._test("test", ["atest", "test", "test2"],
                   "test", False, "test")
        self._test("atest", ["atest", "test", "test2"],
                   "atest", False, "atest")
        self._test("test2", ["atest", "test", "test2"],
                   "test2", False, "test2")

    def test_prefix(self):
        self._test("a", ["test", "test2", "atest"],
                   "atest", False, "a")

    def test_hchoices(self):
        self._test("h", ["test", "test2", "atest"],
                   "hidden", False, "h", hchoices=["hidden"])

    def test_upper(self):
        self._test("A", ["test", "test2", "atest"],
                   "atest", True, "A")
        self._test("T", ["test", "test2", "atest"],
                   "test", True, "T")
        self._test("ATES", ["test", "test2", "atest"],
                   "atest", True, "ATES")
        self._test("teSt2", ["test", "test2", "atest"],
                   "test2", True, "teSt2")


class BaseTest(unittest.TestCase):

    expect_quit = (False, False)

    @classmethod
    def setUpClass(cls):
        if cls is BaseTest:
            raise unittest.SkipTest("base test class")
        super(BaseTest, cls).setUpClass()

    def setUp(self):
        self.ed = Mock(spec=MvEd(VirtualFs()), autospec=True)
        self.menu = Menu(self.ed)
        self.args = (self.menu, self.ed)

    def tearDown(self):
        self.assertEqual(self.expect_quit,
                         (self.menu.do_quit, self.menu.quit_error),
                         "(do_quit, quit_error)")

    def __getattr__(self, name):
        if name == 'applier':
            a = Mock(spec=APPLIER_SPEC, autospec=True)
            self.menu.applier = self.applier = a
            return a
        raise AttributeError


class MenuReadchoiceTest(BaseTest):

    @patch('mved.menu.readchoice')
    def test_simple(self, m_readchoice):
        menu, ed = self.args
        menu._readchoice(["test"])
        self.assertTrue(m_readchoice.called)


class GetChoicesTest(BaseTest):

    def test_unchanged(self):
        menu, ed = self.args
        ed.stats.change_count = 0
        ed.has_conflicts = False
        ed.has_exist_conflicts = False
        choices, hchoices, default = menu._get_choices()
        self.assertEqual(choices, ["edit", "quit"])
        self.assertEqual(hchoices, ["recalc"])
        self.assertEqual(default, "quit")

    def test_changed(self):
        menu, ed = self.args
        ed.stats.change_count = 1
        ed.has_conflicts = False
        ed.has_exist_conflicts = False
        choices, hchoices, default = menu._get_choices()
        self.assertEqual(choices, ["diff", "apply", "dryrun", "edit",
                                   "reset", "quit"])
        self.assertEqual(hchoices, ["recalc"])
        self.assertEqual(default, "quit")

    def test_conflict(self):
        menu, ed = self.args
        ed.stats.change_count = 1
        ed.has_conflicts = True
        ed.has_exist_conflicts = False
        choices, hchoices, default = menu._get_choices()
        self.assertEqual(
            choices, ["conflicts", "diff", "edit", "reset", "quit"])
        self.assertEqual(hchoices, ["recalc"])
        self.assertEqual(default, "quit")

    def test_revert(self):
        menu, ed = self.args
        ed.stats.change_count = 1
        ed.has_conflicts = True
        ed.has_exist_conflicts = False
        self.applier.successful = False
        choices, hchoices, default = menu._get_choices()
        self.assertEqual(choices, ["revert", "dryrun", "quit"])
        self.assertEqual(hchoices, [])
        self.assertIsNone(default)

    def test_review(self):
        menu, ed = self.args
        ed.stats.change_count = 1
        ed.has_conflicts = False
        ed.has_exist_conflicts = False
        self.applier.successful = True
        choices, hchoices, default = menu._get_choices()
        self.assertEqual(choices, ["revert", "dryrun", "quit"])
        self.assertEqual(hchoices, [])
        self.assertEqual(default, "quit")


class HandleChoiceTest(BaseTest):

    def test_diff(self):
        menu, ed = self.args
        menu._handle_choice("diff", False, "")
        ed.write_diff.assert_called_once()


class ChoiceTest(BaseTest):

    @classmethod
    def setUpClass(cls):
        if cls is ChoiceTest:
            raise unittest.SkipTest("base test class")
        super(ChoiceTest, cls).setUpClass()

    def _test_choice(self, choice=None, args="", upper=False):
        menu, ed = self.args
        if choice is None:
            choice = self.choicestr
        self.choice.__func__(menu, choice, args, upper)
        return menu, ed


class ChoiceApplyTest(ChoiceTest):

    choice = Menu.choice_apply
    choicestr = "apply"

    def test_apply(self):
        menu, ed = self.args
        applier = Mock(spec=APPLIER_SPEC)
        applier.error = None
        applier.successful = True
        ed.create_applier.return_value = applier
        with patch('mved.menu.print'):
            self._test_choice()
        ed.create_applier.assert_called_once()
        applier.apply.assert_called_once_with(out=ANY, dryrun=False,
                                              colors=menu.colors)
        self.assertEqual(menu.applier, applier)

    def test_apply_error(self):
        menu, ed = self.args
        applier = Mock(spec=APPLIER_SPEC, autospec=True)
        applier.error = (sentinel.error_arg,)
        applier.num_revert_changes = 1
        applier.successful = False
        applier.get_stats_message.return_value = sentinel.applier_stats
        ed.create_applier.return_value = applier
        with patch('mved.menu.print') as m_print:
            with patch('traceback.print_exception') as m_exc:
                self._test_choice()
                m_print.assert_called_with(sentinel.applier_stats)
                m_exc.assert_called_once_with(sentinel.error_arg)
        ed.create_applier.assert_called_once()
        applier.apply.assert_called_once_with(out=ANY, dryrun=False,
                                              colors=menu.colors)
        self.assertEqual(menu.applier, applier)

    def test_dryrun(self):
        menu, ed = self.args
        applier = Mock(spec=APPLIER_SPEC, autospec=True)
        applier.error = None
        applier.successful = True
        ed.create_applier.return_value = applier
        with patch('mved.menu.print'):
            self._test_choice(choice="dryrun")
        ed.create_applier.assert_called_once()
        applier.apply.assert_called_once_with(out=ANY, dryrun=True,
                                              colors=menu.colors)
        self.assertIsNone(menu.applier, None)

    def test_revert_error(self):
        menu, ed = self.args
        applier = Mock(spec=APPLIER_SPEC, autospec=True)
        error = (object(),)
        applier.error = error
        applier.successful = False
        self.applier.parent = None
        self.applier.create_revert_context.return_value = applier
        with patch('mved.menu.print'):
            with patch('traceback.print_exception') as m_exc:
                self._test_choice(choice="revert")
                m_exc.assert_called_once_with(*error)
        ed.create_applier.assert_not_called()
        self.applier.create_revert_context.assert_called_once()
        applier.apply.assert_called_once_with(out=ANY, dryrun=False,
                                              colors=menu.colors)
        self.assertIs(menu.applier, applier)

    def test_revert(self):
        menu, ed = self.args
        applier = Mock(spec=APPLIER_SPEC, autospec=True)
        applier.error = None
        applier.successful = True
        self.applier.parent = None
        self.applier.create_revert_context.return_value = applier
        with patch('mved.menu.print'):
            self._test_choice(choice="revert")
        ed.create_applier.assert_not_called()
        self.applier.create_revert_context.assert_called_once()
        applier.apply.assert_called_once_with(out=ANY, dryrun=False,
                                              colors=menu.colors)
        self.assertIsNone(menu.applier)


class ChoiceEditTest(ChoiceTest):

    choice = Menu.choice_edit
    choicestr = "edit"

    def test_simple(self):
        progstr = "---test---"
        menu, ed = self._test_choice(args=progstr)
        ed.start_editor.assert_called_once_with(
            reset=False, sources=True, program=progstr, stream=False)

    def test_stream(self):
        progstr = "---test---"
        menu, ed = self._test_choice(args="| " + progstr)
        ed.start_editor.assert_called_once_with(
            reset=False, sources=True, program=progstr, stream=True)

    def test_upper(self):
        menu, ed = self._test_choice(upper=True)
        ed.start_editor.assert_called_once_with(
            reset=False, sources=False, program=None, stream=False)


class ChoiceResetTest(ChoiceTest):

    choice = Menu.choice_reset
    choicestr = "reset"

    def test_simple(self):
        progstr = "---test---"
        menu, ed = self._test_choice(args=progstr)
        ed.start_editor.assert_called_once_with(
            reset=True, sources=ANY, program=progstr, stream=False)
        self.assertEqual(len(ed.mock_calls), 1, ed.mock_calls)

    def test_upper(self):
        with patch('mved.menu.print'):
            menu, ed = self._test_choice(upper=True)
        ed.reset.assert_called_once()
        ed.start_editor.assert_not_called()


class ChoiceDiffTest(ChoiceTest):

    choice = Menu.choice_diff
    choicestr = "diff"

    def test_simple(self):
        menu, ed = self._test_choice()
        ed.write_diff.assert_called_once()
        name, args, kw = ed.write_diff.mock_calls[0]
        self.assertFalse(kw['numbers'])
        self.assertEqual(len(ed.mock_calls), 1, ed.mock_calls)

    def test_upper(self):
        menu, ed = self._test_choice(upper=True)
        ed.write_diff.assert_called_once()
        name, args, kw = ed.write_diff.mock_calls[0]
        self.assertTrue(kw['numbers'])
        self.assertEqual(len(ed.mock_calls), 1, ed.mock_calls)


class ChoiceAddTest(ChoiceTest):

    choice = Menu.choice_add
    choicestr = "add"

    def test_simple(self):
        menu, ed = self._test_choice()
        ed.add_exist_conflict_files.assert_called_once()


class ChoiceConflictsTest(ChoiceTest):

    choice = Menu.choice_conflicts
    choicestr = "conflicts"

    def test_simple(self):
        menu, ed = self._test_choice()
        ed.write_conflicts.assert_called_once_with(ANY, short=True,
                                                   colors=menu.colors)

    def test_upper(self):
        menu, ed = self._test_choice(upper=True)
        ed.write_conflicts.assert_called_once_with(ANY, short=False,
                                                   colors=menu.colors)


class ChoiceRecalcTest(ChoiceTest):

    choice = Menu.choice_recalc
    choicestr = "recalc"

    def test_simple(self):
        menu, ed = self._test_choice()


class ChoiceQuitTest(ChoiceTest):

    choice = Menu.choice_quit
    choicestr = "quit"

    def test_simple(self):
        menu, ed = self._test_choice()
        self.expect_quit = (True, False)
        self.assertEqual(ed.mock_calls, [])

    @patch('mved.menu.readchoice')
    def _test_applier(self, success, choice, quit, m_readchoice):
        menu, ed = self.args
        menu.applier = Mock(spec=ApplyContext, autospec=True)
        menu.applier.successful = success
        if choice:
            m_readchoice.return_value = choice
        else:
            m_readchoice.side_effect = AssertionError
        self._test_choice()
        if choice:
            m_readchoice.assert_called_once()
            self.assertEqual(m_readchoice.call_args[0][0], ["yes", "no"])
        self.expect_quit = (quit, quit and not success)
        self.assertEqual(ed.mock_calls, [])
        return menu, ed

    def test_revertmode_yes(self):
        menu, ed = self._test_applier(False, ("yes", False, "y"), True)
        self.assertEqual(menu.applier.mock_calls, [])

    def test_revertmode_no(self):
        menu, ed = self._test_applier(False, ("yes", False, "y"), True)
        self.assertEqual(menu.applier.mock_calls, [])

    def test_reviewmode(self):
        menu, ed = self._test_applier(True, (), True)
        menu.applier.delete_now.assert_called_once()
        self.assertEqual(len(menu.applier.mock_calls), 1,
                         menu.applier.mock_calls)


# vim:set sw=4 et:
