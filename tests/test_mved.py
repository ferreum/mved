# This file is part of mved, the bulk file renaming tool.
# License: GNU GPL version 3, see the file "AUTHORS" for details.

import unittest

import mved
from mved import (FileNondirExistsConflict, FileExistsConflict, DestConflict)
import tests.virtualfs as vfs
import mved.utils as utils
from mved.editor import Editor
from mved.files import SrcFile


SILENT = True


class BaseTest(unittest.TestCase):

    def setUp(self):
        fs = vfs.VirtualFs()
        self.ed = mved.MvEd(fs)
        self.args = self.ed, fs
        if SILENT:
            self.output = None
        else:
            import sys
            self.output = sys.stdout

    def set_dests(self, destss):
        self.assertGreaterEqual(len(self.ed.files), len(destss))
        for dests, file in zip(destss, self.ed.files):
            file.dests = dests
        self.ed.calculate_changes()

    def apply_ed(self, delete_on_success=True):
        ed, fs = self.args
        applier = ed.create_applier()
        applier.delete_on_success = delete_on_success
        applier.apply(out=self.output)
        if applier.error:
            raise applier.error[1].with_traceback(applier.error[2])
        return applier

    def _verify_stats(self, stats):
        for k in ('conflicts', 'copies', 'moves', 'deletes', 'temp_moves'):
            self.assertEqual(getattr(self.ed.stats, k), stats.get(k, 0), k)


class BaseTestSetup(BaseTest):

    files = ()
    dirs = ()

    def setUp(self):
        BaseTest.setUp(self)
        ed, fs = self.args
        self.created = []
        for f in self.files:
            fs.makedirs(fs.dirname(f), exist_ok=True)
            self.created.append(fs.mkfile(f))
        for f in self.dirs:
            self.created.append(fs.makedirs(f, exist_ok=True))
        self.assertEqual(len(set(self.created)), len(self.created))

    def _test_moved(self, destss, stats={}):
        ed, fs = self.args
        destss = [(d,) if isinstance(d, str) else d for d in destss]
        ed.add_files_raw(self.files)
        ed.apply_sourceset()
        self.set_dests(destss)
        if stats.get('conflicts', 0) == 0 and ed.stats.conflicts == 0:
            self.apply_ed()
            for src, srcf, dests in zip(self.files, self.created, destss):
                found_orig = False
                for dest in dests:
                    self.assertEqual(fs.handle(dest).name, srcf.name)
                    if src == dest:
                        self.assertIs(fs.handle(src), srcf, "file has changed")
                    if srcf is fs.handle(dest):
                        found_orig = True
                if dests and not found_orig:
                    assert False, "only copies found"
        for conflict in ed.conflicts:
            self.assertNotEqual(None, conflict.get_message(short=True))
            self.assertNotEqual(None, conflict.get_message(short=False))
        for change in ed.changeset.changes:
            self.assertNotEqual(None, change.get_message())
        self._verify_stats(stats)


class FewFiles(object):

    files = ("a1", "a2", "a3")


class SimpleTest(FewFiles, BaseTestSetup):

    def test_base(self):
        self._test_moved(["x1", "x2", "a3"], {'moves': 2})


class DestConflictTest(FewFiles, BaseTestSetup):

    def test_dest(self):
        ed, fs = self.args
        self._test_moved(["x1", "x1", "a3"], {'conflicts': 1, 'moves': 2})
        self.assertEqual(type(ed.conflicts[0]), mved.DestConflict)
        self.assertEqual(ed.conflicts[0].dest, "/x1")

    def test_circle_resolved(self):
        self._test_moved(["a2", "a3", "a1"], {'temp_moves': 1, 'moves': 3})

    def test_circle_and_dest(self):
        ed = self.ed
        self._test_moved(["a2", "a1", "a1"], {'conflicts': 1, 'moves': 3})
        self.assertEqual(type(ed.conflicts[0]), mved.DestConflict)
        self.assertEqual(ed.conflicts[0].dest, "/a1")


class DirConflictTest(BaseTestSetup):

    files = ("/aa/a1",)
    dirs = ("/aa", "/bb")

    def verify_dirconflict(self, path, is_source, files=files):
        errors = []
        for conflict in self.ed.conflicts:
            try:
                self.assertEqual(mved.DirConflict, type(conflict))
                self.assertEqual(path, conflict.file.src)
                self.assertEqual(is_source, conflict.is_source)
                self.assertEqual(set(files),
                                 set(f.src for f in conflict.files))
                break
            except AssertionError as e:
                errors.append(e)
        else:
            raise AssertionError("No conflict matches:\n" + "\n".join(
                str(e) for e in errors))

    def test_move_parent_of_file_src(self):
        ed, fs = self.args
        ed.add_files_raw(self.dirs)
        self._test_moved(["/xx", "/bb", "/bb/a1"],
                         {'conflicts': 1, 'moves': 2})
        self.verify_dirconflict("/aa", True)

    def test_move_parent_of_file_dest(self):
        ed, fs = self.args
        ed.add_files_raw(self.dirs)
        self._test_moved(["/aa", "/xx", "/bb/a1"],
                         {'conflicts': 1, 'moves': 2})
        self.verify_dirconflict("/bb", False)

    def test_move_to_parent_of_file(self):
        ed, fs = self.args
        ed.add_files_raw(["/bb"])
        self._test_moved(["/aa", "/aa/a1"],
                         {'conflicts': 2, 'moves': 1})
        self.verify_dirconflict("/bb", False)
        self.verify_dirconflict("/bb", True)


class LargeCircleTest(BaseTest):

    def setUp(self):
        pass

    def test_large_circle_resolved(self):
        for num in (2, 20, 200, 2000):
            BaseTest.setUp(self)
            ed, fs = self.args
            with self.subTest(num=num):
                files = []
                dests = []
                for i in range(num):
                    name = "/" + str(i)
                    fs.mkfile(name)
                    files.append(name)
                    dests.append((name,))
                dests.insert(0, dests.pop(-1))
                self.assertEqual(len(files), num)
                self.assertEqual(len(dests), num)

                ed.add_files_raw(files)
                ed.apply_sourceset()
                self.set_dests(dests)

                self._verify_stats(
                    {'moves': num, 'temp_moves': 1, 'conflicts': 0}
                )

                self.apply_ed()

                for src, dests in zip(files, dests):
                    self.assertEqual(src, fs.handle(dests[0]).name)


class CircleMultiParentTest(BaseTestSetup):

    files = ("a1", "a2", "b1", "b2")

    def test_circle_copy(self):
        self._test_moved(
                [("a2", "b2"), ("a1", "b1"), (), ()],
                {'moves': 2, 'copies': 2, 'deletes': 2, 'temp_moves': 1})


class CircleMultiParent2Test(BaseTestSetup):

    files = ("a1", "a2", "a3", "b1", "b2", "b3")

    def test_circle_copy(self):
        self._test_moved(
                [("a2", "b2"), ("a3", "b3"), ("a1", "b1"),
                    (), (), ()],
                {'moves': 3, 'copies': 3, 'deletes': 3, 'temp_moves': 1})


class FileExistsConflictTest(FewFiles, BaseTestSetup):

    def test_simple(self):
        ed, fs = self.args
        self.files = self.files[0:1]
        self._test_moved(["a2"], {'conflicts': 1, 'moves': 1})
        self.assertTrue(self.ed.has_exist_conflicts)
        self.assertEqual(type(ed.conflicts[0]), FileExistsConflict)

    def test_to_parent_exists_nondir(self):
        ed, fs = self.args
        self.files = self.files[0:1]
        self._test_moved(["a2/a1"], {'conflicts': 1, 'moves': 1})
        self.assertTrue(ed.has_exist_conflicts)
        self.assertEqual(type(ed.conflicts[0]), FileNondirExistsConflict)

    def test_to_parent_exists_nonexlink(self):
        ed, fs = self.args
        fs.symlink('x1', 'm1')
        self.files = self.files[0:1]
        self._test_moved(["m1/a1"], {'conflicts': 0, 'moves': 1})

    def test_move_to_under_added_file(self):
        ed, fs = self.args
        self._test_moved(["a1", "a1/a2", "a3"], {'conflicts': 1, 'moves': 1})
        self.assertEqual(type(ed.conflicts[0]), DestConflict)


class DeleteTest(BaseTestSetup):

    files = ("/aa/a1", "/bb/b2", "/aa/cc/c1")

    def test_file(self):
        ed, fs = self.args
        ed.add_files_raw(self.files)
        ed.apply_sourceset()
        self.set_dests([(), (), ()])
        self.apply_ed()
        self.assertIs(fs.handle("/aa/a1"), vfs.NONEFILE)
        self._verify_stats({'deletes': 3})


class CopyTest(FewFiles, BaseTestSetup):

    files = ("a1", "a2", "a3")

    def test_file(self):
        files = self.files
        self._test_moved([
            (files[0], "/xx/x1"),
            (files[1], "/xx/x2"),
            (files[2], "/x3")], {'copies': 3})

    def test_file_with_move(self):
        self._test_moved([
            ("/yy/y1", "/aa/xx/x1"),
            ("/yy/y2", "/aa/xx/x2"),
            ("/yy/y3", "/aa/x3")],
            {'copies': 3, 'moves': 3})

    def test_conflict(self):
        self._test_moved([
            ("/yy/y1", "/aa/xx/x1"),
            ("/yy/y1", "/aa/xx/x1"),
            ("/yy/y3", "/yy/y1")],
            {'conflicts': 2, 'copies': 3, 'moves': 3})

    def test_circle_resolved(self):
        self._test_moved([
            ("/yy/y1", "/a2"),
            ("/yy/y2", "/a3"),
            ("/yy/y3", "/a1")],
            {'copies': 3, 'moves': 3, 'temp_moves': 1})

    def test_src_conflict(self):
        self._test_moved([
            ("/a1", "/x1"),
            ("/a2", "/a1"),
            ("/a3")],
            {'conflicts': 1, 'copies': 2})

    def test_circle_move_del(self):
        self._test_moved([
            ("a3", "a2"),
            ("a1"),
            ()],
            {'copies': 1, 'moves': 2, 'deletes': 1, 'temp_moves': 1})


class DirsTest(FewFiles, BaseTestSetup):

    files = ("/aa/a1", "/bb/b2", "/aa/cc/c1")

    def test_into_new(self):
        self._test_moved(["/xx/x1", "/yy/y2", "/xx/y2"], {'moves': 3})

    def test_circle_resolved(self):
        self._test_moved(
            ["/bb/b2", "/aa/cc/c1", "/aa/a1"], {'moves': 3, 'temp_moves': 1}
        )


class DirMoveTest(BaseTestSetup):

    files = ("/aa/a1", "/bb/b1", "/cc/c1")
    dirs = ("/aa", "/bb", "/cc")

    def setUp(self):
        BaseTestSetup.setUp(self)
        ed, fs = self.args
        ed.add_files_raw(self.dirs)
        ed.apply_sourceset()

    def test_circle_resolved(self):
        ed, fs = self.args
        self.set_dests([("/bb",), ("/cc",), ("/aa",)])
        self.apply_ed()
        for i, f in enumerate(("/bb/a1", "/cc/b1", "/aa/c1")):
            self.assertIs(fs.handle(f), self.created[i])
        self._verify_stats({'moves': 3, 'temp_moves': 1})

    def test_copy_disallowed(self):
        ed, fs = self.args
        self.set_dests([("/aa", "/xx"), ("/bb",), ("/cc",)])
        self._verify_stats({'conflicts': 1, 'copies': 1})

    def test_delete_disallowed(self):
        ed, fs = self.args
        self.set_dests([(), ("/bb",), ("/cc",)])
        self._verify_stats({'conflicts': 1, 'deletes': 1})


class RevertTest(BaseTestSetup):

    files = ("/a1", "/a2", "/a3")

    def _test_revert(self, destss):
        ed, fs = self.args
        ed.add_files_raw(self.files)
        ed.apply_sourceset()
        self.set_dests(destss)
        applier = self.apply_ed(delete_on_success=False)
        revert = applier.create_revert_context()
        revert.apply(out=self.output)
        if revert.error:
            raise revert.error[1].with_traceback(revert.error[2])

    def test_move(self):
        ed, fs = self.args
        destss = [("/x1",), ("/x2",), ("/x3",)]
        self._test_revert(destss)
        for name, created, dests in zip(self.files, self.created, destss):
            self.assertEqual(fs.handle(name), created)
            self.assertIs(fs.handle(dests[0]), vfs.NONEFILE)

    def test_copy_move(self):
        ed, fs = self.args
        destss = [("x1", "y1"), ("x2", "y2"), ("x3", "y3")]
        self._test_revert(destss)
        for name, created, dests in zip(self.files, self.created, destss):
            self.assertEqual(fs.handle(name), created)
            for dest in dests:
                self.assertIs(fs.handle(dest), vfs.NONEFILE)
        self.assertEqual(set(map(fs.handle, fs.listdir("/"))),
                         set(self.created))

    def test_delete(self):
        ed, fs = self.args
        self._test_revert([(), (), ()])
        for name, created in zip(self.files, self.created):
            self.assertEqual(fs.handle(name), created)
        self.assertEqual(set(map(fs.handle, fs.listdir("/"))),
                         set(self.created))


class InvalidNameTest(FewFiles, BaseTestSetup):

    invalid_names = (
        ".", "./", "../.", ".././", "..", "./..", "", "/", "//")

    def test_dot_dest(self):
        ed, fs = self.args
        ed.add_files_raw(self.files)
        ed.apply_sourceset()
        for dest in self.invalid_names:
            with self.subTest(dest=dest):
                self.set_dests([(dest,)] + list(self.files[1:]))
                self._verify_stats({'conflicts': 1})

    def test_dot_src(self):
        ed, fs = self.args
        ed.dir_levels = 0
        for name in self.invalid_names:
            with self.subTest(name=name):
                with self.assertRaises(mved.SourceFilenameError):
                    ed.add_files(name)
                self.assertEqual(ed.sourceset.files, [])


class EscapeTest(unittest.TestCase):

    def test_both(self):
        args = ((r'abcdef', 'abcdef'),
                (r'\\\a\b\f\n\r\t\v', '\\\a\b\f\n\r\t\v'),
                (r'\xff', '\udcff'),
                (r'\x7f', '\x7f'),
                (r'\\\x10', '\\\x10'))
        for e, u in args:
            self.assertEqual(
                (u, e),
                (utils.unescapepath(e), utils.escapepath(u))
            )

    def test_escape(self):
        self.assertEqual(utils.escapepath('\x7f'), r'\x7f')
        self.assertEqual(utils.escapepath('\n'), r'\n')
        self.assertEqual(utils.escapepath('\t'), r'\t')
        self.assertEqual(utils.escapepath('\udc80'), r'\x80')
        self.assertEqual(utils.escapepath('\udcff'), r'\xff')

    def test_unescape(self):
        self.assertEqual(utils.unescapepath(r'\\\x5c'), r'\\')
        self.assertEqual(utils.unescapepath(r'\\\\x5c'), r'\\x5c')
        self.assertEqual(utils.unescapepath(r'\x321'), '21')
        self.assertEqual(utils.unescapepath(r'\xfff'), '\udcfff')
        self.assertEqual(utils.unescapepath(r'\ \z\.\x\0'), ' z.x0')

    def test_inverses(self):
        for c in range(1, 0xff):
            self.assertEqual(chr(c),
                             utils.unescapepath(utils.escapepath(chr(c))))

    def test_surrogates(self):
        for c in range(0x80, 0xff):
            surr = chr(0xdc00 | c)
            self.assertEqual(f"\\x{c:02x}", utils.escapepath(surr))
            self.assertEqual(surr, utils.unescapepath(f"\\x{c:02x}"))


class FormatFilesTest(unittest.TestCase):

    def _test(self, files, output):
        self.assertEqual(
            utils.format_files(files, lim_before=3, lim_after=1),
            output)

    def test_many(self):
        self._test(["a", "b", "c", "d", "e"], "a, b, c, [+1 files], e")
        self._test(["a", "b", "c", "d", "e", "f"], "a, b, c, [+2 files], f")

    def test_on_limit(self):
        self._test(["a", "b", "c", "d"], "a, b, c, d")

    def test_few(self):
        self._test(["a", "b", "c"], "a, b, c")
        self._test(["a", "b"], "a, b")
        self._test(["a"], "a")
        self._test([""], "")


class WriteDiffTest(unittest.TestCase):

    def test_write(self):
        file = SrcFile(1, "aa", ("bb",))
        file.get_diff(numbers=False)
        file.get_diff(numbers=True)


class EditorReadTest(unittest.TestCase):

    def _test_read(self, srcs, lines, expectdestss):
        srcfiles = [SrcFile(n, src) for n, src in enumerate(srcs, start=1)]
        edit = Editor(srcfiles)
        files = edit._read_file(lines)
        self.assertEqual([f.src for f in files], srcs)
        self.assertEqual([f.dests for f in files], expectdestss)

    def test_unchanged(self):
        self._test_read(["aa"], ["1 aa"], [["aa"]])
        self._test_read(["aa"], [" 1 aa"], [["aa"]])
        self._test_read(["aa", "bb"], [" 1 aa", "\t2 bb"], [["aa"], ["bb"]])

    def test_move(self):
        self._test_read(["aa"], ["1 xx"], [["xx"]])
        self._test_read(["aa", "bb"], ["1 xx", "2 yy"], [["xx"], ["yy"]])

    def test_move_copy(self):
        self._test_read(["aa"], ["1 xx", "1 yy"], [["xx", "yy"]])
        self._test_read(["aa", "bb"], ["1 xx", "1 yy", "2 zz", "2 ww"],
                        [["xx", "yy"], ["zz", "ww"]])
        self._test_read(["aa", "bb"], ["1 xx", "2 zz", "2 ww", "1 yy"],
                        [["xx", "yy"], ["zz", "ww"]])

    def test_delete(self):
        self._test_read(["aa"], [], [[]])
        self._test_read(["aa", "bb", "cc", "dd"], [], [[], [], [], []])
        self._test_read(["aa", "bb", "cc", "dd"], ["3 cc"],
                        [[], [], ["cc"], []])

    def test_whitespace(self):
        self._test_read(["aa"], ["1  aa "], [[" aa "]])
        self._test_read(["aa"], [" 1  aa "], [[" aa "]])
        self._test_read(["aa"], ["\t1 \taa "], [["\taa "]])

    def test_excapes(self):
        self._test_read(["aa"], [r"1 \x20aa\x20"], [[" aa "]])
        self._test_read(["aa"], [r"1 \naa"], [["\naa"]])
        self._test_read(["aa"], [r"1 \ra\fa"], [["\ra\fa"]])

    def test_comments(self):
        self._test_read(["aa"], ["1 aa", "", "1 xx"], [["aa", "xx"]])
        self._test_read(["aa"], ["1 aa", "# test", "1 xx"], [["aa", "xx"]])
        self._test_read(["aa"], ["1 aa", " # test", "1 xx"], [["aa", "xx"]])

    def test_leading_zero(self):
        self._test_read(["aa"], ["01 aa"], [["aa"]])
        self._test_read(["aa"], ["  00001 aa"], [["aa"]])


class MockFile(object):

    def __init__(self, buffer=None):
        from cStringIO import StringIO
        self.buffer = StringIO()

    def write(self, b):
        self.buffer.extend(b)


class EditorWriteTest(unittest.TestCase):

    def _test_write(self, srcs, destss, expectlines,
                    reset=False, sources=True):
        expect = expectlines + ['']
        srcfiles = [SrcFile(n, src, dests=dests)
                    for n, (src, dests)
                    in enumerate(zip(srcs, destss), start=1)]
        edit = Editor(srcfiles)
        from io import StringIO
        out = StringIO()
        edit._write_file(out, reset, sources)
        self.assertEqual(out.getvalue().split('\n'), expect)

    def test_unchanged(self):
        self._test_write(["aa"], [["aa"]], ["1 aa"])
        self._test_write(["aa", "bb"], [["aa"], ["bb"]], ["1 aa", "2 bb"])

    def test_move(self):
        self._test_write(["aa"], [["xx"]], ["# 1 aa", "1 xx"])
        self._test_write(["aa", "bb"], [["xx"], ["yy"]],
                         ["# 1 aa", "1 xx", "# 2 bb", "2 yy"])

    def test_copy_move(self):
        self._test_write(["aa"], [["xx", "yy"]], ["# 1 aa", "1 xx", "1 yy"])
        self._test_write(["aa", "bb"], [["xx", "yy"], ["zz", "ww"]],
                         ["# 1 aa", "1 xx", "1 yy", "# 2 bb", "2 zz", "2 ww"])

    def test_delete(self):
        self._test_write(["aa"], [[]], ["# 1 aa"])
        self._test_write(["aa", "bb"], [[], []], ["# 1 aa", "# 2 bb"])
        self._test_write(["aa", "bb", "cc", "dd"], [[], [], [], []],
                         ["# 1 aa", "# 2 bb", "# 3 cc", "# 4 dd"])
        self._test_write(["aa", "bb", "cc", "dd"], [[], [], ["cc"], []],
                         ["# 1 aa", "# 2 bb", "3 cc", "# 4 dd"])

    def test_reset(self):
        self._test_write(["aa", "bb"], [["xx"], ["yy"]],
                         ["1 aa", "2 bb"], reset=True)
        self._test_write(["aa", "bb"], [[], []],
                         ["1 aa", "2 bb"], reset=True)

    def test_nosrc(self):
        self._test_write(["aa", "bb"], [["xx"], ["yy"]],
                         ["1 xx", "2 yy"], sources=False)
        self._test_write(["aa", "bb"], [[], []],
                         [], sources=False)

    def test_many(self):
        self._test_write(
            ["a", "b", "c", "d", "e", "f",
             "g", "h", "i", "j", "k", "l"],
            [["a"], ["b"], ["c"], ["d"], ["e"], ["f"],
             ["g"], ["h"], ["i"], ["j"], ["k"], ["l"]],
            ["01 a", "02 b", "03 c", "04 d", "05 e", "06 f",
             "07 g", "08 h", "09 i", "10 j", "11 k", "12 l"])


class EditorEditTest(unittest.TestCase):

    def _test_edit(self, srcs, destss,
                   program=None, stream=False, origdestss=None, reset=False):
        if origdestss is None:
            origdestss = [None] * len(srcs)
        srcfiles = [SrcFile(n, src, dests=dests)
                    for n, (src, dests)
                    in enumerate(zip(srcs, origdestss), start=1)]
        edit = Editor(srcfiles)
        edit.run_editor(program=program, stream=stream, reset=reset,
                        do_log=False)
        self.assertEqual([f.src for f in edit.files], srcs)
        self.assertEqual([f.dests for f in edit.files], destss)

    def test_edit_unchanged(self):
        self._test_edit(["aa", "bb"], [["aa"], ["bb"]],
                        program="true", stream=False)

    def test_edit_change(self):
        self._test_edit(["aa", "bb"], [["xx"], ["yy"]],
                        program="bash -c 'echo 1 xx; echo 2 yy; true' >",
                        stream=False)

    def test_edit_abort(self):
        self._test_edit(["aa", "bb"], [["aa"], ["bb"]],
                        program="bash -c 'echo 1 xx; echo 2 yy; false' >",
                        stream=False)

    def test_stream_unchanged(self):
        self._test_edit(["aa", "bb"], [["aa"], ["bb"]],
                        program="cat", stream=True)

    def test_stream_change(self):
        self._test_edit(["aa", "bb"], [[], []],
                        program="true", stream=True)
        self._test_edit(["aa", "bb"], [["xx"], ["yy"]],
                        program="echo 1 xx; echo 2 yy; true", stream=True)

    def test_stream_abort(self):
        self._test_edit(["aa", "bb"], [["aa"], ["bb"]],
                        program="false", stream=True)

    def test_reset(self):
        self._test_edit(["aa"], [["xx"]], origdestss=[["yy"]],
                        program="echo 1 xx >", stream=False, reset=True)

    def test_reset_unchanged(self):
        self._test_edit(["aa"], [["xx"]], origdestss=[["xx"]],
                        program="true", stream=False, reset=True)


# vim:set sw=4 et:
