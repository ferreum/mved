" This file is part of mved, the bulk file renaming tool.
" License: GNU GPL version 3, see the file "AUTHORS" for details.
"
" File:        ftdetect/mved.vim
" Description: Vim filetype detection for mved, the bulk file renaming tool.

autocmd BufRead,BufNewFile *.mved set filetype=mved
