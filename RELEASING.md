# build env setup
1. install `build`: `pip install build`

# bump version
1. change `mved/_version.py`
2. commit with message `version <version>`
3. run `git tag version-<version>`

# run build
1. `python -m build`

# install
1. run `pip install dist/mved-x.x-*.whl`
